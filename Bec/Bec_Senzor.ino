//cu un led conectat la pinul 13 simulam un bec
const int outpin = 13;              
const int inpin = A0; 
int contor;
void setup()
{ 
  Serial.begin(115200);
  Serial.println("Start");
  pinMode(outpin, OUTPUT);
  digitalWrite(outpin, LOW);
}

void loop()
{
  //citim datele de la senzor
  int val=analogRead(inpin);
  Serial.println(val);
  
  //in cazul in care val trece de 125 aprindem becul pentru 10s
  if (val >= 125)			
  {
    digitalWrite(outpin, HIGH);
    contor = 100;
    while (contor--)
    {
      Serial.println(contor);
      //reactualizam val pentru a verifica daca pe parcursul celor 10s a mai fost actionat senzorul
      val = analogRead(inpin);
      if (val >= 125)
	//reinitializam contorul
        contor = 100;
      delay(100);
    }
  }
  digitalWrite(outpin, LOW);
}

